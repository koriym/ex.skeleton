<?php
/**
 * This file is part of the Ex.Skeleton package
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Ex\Skeleton\Module;

use Aura\Sql\ExtendedPdo;
use Ray\Di\ProviderInterface;

class ExtendedPdoProdProvider implements ProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function get()
    {
        $pdo = new ExtendedPdo(
            $_ENV['APP_DB_DSN'],
            $_ENV['APP_DB_USER'],
            $_ENV['APP_DB_PASS']
        );

        return $pdo;
    }
}
