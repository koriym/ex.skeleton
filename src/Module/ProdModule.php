<?php
/**
 * This file is part of the Ex.Skeleton  package
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Ex\Skeleton\Module;

use Ray\Di\AbstractModule;
use Aura\Sql\ExtendedPdoInterface;
use Ray\Di\Scope;
use BEAR\Package\Context\ProdModule as PackageProdModule;

class ProdModule extends AbstractModule
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        // aura/sql
        $this->bind(ExtendedPdoInterface::class)->toProvider(ExtendedPdoProdProvider::class)->in(Scope::SINGLETON);
        // bear/package
        $this->install(new PackageProdModule);
    }
}
