<?php
/**
 * This file is part of the Ex.Skeleton  package
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Ex\Skeleton\Module;

use Aura\Sql\ExtendedPdo;
use Ray\Di\ProviderInterface;

class ExtendedPdoProvider implements ProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function get()
    {
        $conf = dirname(dirname(__DIR__)) . '/var/db';
        $pdo = new ExtendedPdo(
            'sqlite:' . $conf . '/db.sqlite'
        );

        return $pdo;
    }
}
