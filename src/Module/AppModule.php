<?php

namespace Ex\Skeleton\Module;

use BEAR\Package\AppMeta;
use BEAR\Package\PackageModule;
use Ray\Di\AbstractModule;
use Aura\Sql\ExtendedPdoInterface;
use Ray\Di\Scope;

class AppModule extends AbstractModule
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        // aura/sql
        $this->bind(ExtendedPdoInterface::class)->toProvider(ExtendedPdoProvider::class)->in(Scope::SINGLETON);

        // bear/package
        $this->install(new PackageModule(new AppMeta('Ex\Skeleton')));
    }
}
