<?php

namespace Ex\Skeleton\Resource\App;

use Aura\Sql\ExtendedPdoInterface;
use BEAR\Resource\ResourceObject;
use BEAR\Resource\Annotation\Link;

class Greeting extends ResourceObject
{
    /**
     * @var ExtendedPdoInterface
     */
    private $pdo;

    public function __construct(ExtendedPdoInterface $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param string $name
     *
     * @Link(rel="ja", href="/ja/greeting?name={name}")
     */
    public function onGet($name = "BEAR.Sunday")
    {
        $this['greeting'] = "Hello {$name}";
        $this['name'] = $name;

        return $this;
    }
}
